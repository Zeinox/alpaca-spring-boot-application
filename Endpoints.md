# Rest Endpoints
## /
Redirects to /account
## /account
Displays an html page that serves as the main gui of the app

# RSS Endpoints
Updates a received every 5 seconds.

## /account
## /topic/account
Handles pulling and posting account details to Stomp endpoint on the frontend

## /addToWatchlist
## /topic/watchlist
Handles adding stocks to the watchlist

## /popWatchlist
## /topic/watchlist
handle popping stocks off the watchlist