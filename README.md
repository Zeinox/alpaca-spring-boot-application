# tRaid: an alpaca.markets trading interface

Contacts:
 [wichael.malsh@gmail.com](wichael.malsh@gmail.com)
 
Requirements:
* Java 1.8
* [Maven](http://maven.apache.org/download.cgi)
* API keys from [alpaca.markets](https://app.alpaca.markets/login)



Run the application through the command line using:

    mvn clean compile exec:java
   
and then visit [localhost:8080/](localhost:8080/) with your browser of choice when it finishes starting up.
   
Refer to the [official Maven documentation](http://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html) for further clarification on how to run tests or do a clean install


## What is tRaid?

tRaid is a web app that interfaces with [alpaca.markets](https://app.alpaca.markets/login) trade simulation interface.

With further development it can serve as a tool for those unfamiliar with stock trading to get their feet wet and trade stocks at current market value without putting any money in.

Currently the application allows users with an api key(it is free to sign up for on their site), to add stocks to a watchlist and view updates in realtime.

[Documentation](Documentation.md)