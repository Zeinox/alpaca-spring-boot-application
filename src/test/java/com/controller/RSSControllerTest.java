package com.controller;

import com.domain.Message;
import com.domain.Response;
import com.service.StockLookupService;
import com.service.StockWatchListService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RSSControllerTest {

    @Mock
    StockLookupService stockLookupService;

    @Mock
    StockWatchListService stockWatchListService;

    @Mock
    SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    @InjectMocks
    RSSController RSSController;


    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        Mockito.when(stockWatchListService.getWatchlist()).thenReturn(Arrays.asList("GOOGL"));

    }

    @Test
    public void testAddToWatchlist(){
        Message message = new Message();
        message.setName("GOOGL");
        Response response = RSSController.addToWatchList(message);
        Assert.assertEquals("[ \"GOOGL\" ]", response.getContent());
    }

    @Test
    public void testPopWatchlist(){
        Message message = new Message();
        message.setName("GOOGL");
        RSSController.addToWatchList(message);
        Mockito.when(stockWatchListService.getWatchlist()).thenReturn(Arrays.asList(""));

        Response response = RSSController.popWatchList();
        Assert.assertEquals("[ \"\" ]", response.getContent());
    }
}
