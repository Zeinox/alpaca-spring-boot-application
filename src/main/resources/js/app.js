
var stompClient = {};

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/gs-guide-websocket');
    stompClient["watchlist"] = Stomp.over(socket);
    stompClient["watchlist"].connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient["watchlist"].subscribe('/topic/watchlist', function (greeting) {
            showWatchlist(JSON.parse(greeting.body).content);
        });
    });

    var socket = new SockJS('/gs-guide-websocket');
    stompClient["account"] = Stomp.over(socket);
    stompClient["account"].connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient["account"].subscribe('/topic/account', function (greeting) {
            showAccount(JSON.parse(greeting.body).content);
        });
    });

    var socket = new SockJS('/gs-guide-websocket');
    stompClient["watchlistUpdates"] = Stomp.over(socket);
    stompClient["watchlistUpdates"].connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient["watchlistUpdates"].subscribe('/topic/watchlistUpdates', function (greeting) {
            showWatchlistUpdates(JSON.parse(greeting.body).content);
        });
    });
}

function disconnect() {
    $.each(stompClient, function(index, item) {
        if (item !== null) {
            item.disconnect();
        }
    });
    setConnected(false);
    $("#account").html("");
    console.log("Disconnected");
}

function sendWatchlistAddition() {
    stompClient["watchlist"].send("/app/addToWatchlist", {}, JSON.stringify({'name': $("#stock").val()}));
}

function popWatchlistAddition() {
    stompClient["watchlist"].send("/app/popWatchlist", {}, '');
}

function showWatchlist(message) {
    $("#watchlist").html("<pre>" + message  + "</pre>");
}

function showWatchlistUpdates(message) {
    $("#watchlistUpdates").html("<pre>" + message  + "</pre>");
}

function showAccount(message) {
    $("#account").html("<pre>" + message  + "</pre>");
}

$( document ).ready(function() {
    console.log( "ready!" );
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#popWatchList" ).click(function() { popWatchlistAddition(); });
    $( "#sendWatchList" ).click(function() { sendWatchlistAddition(); });
});