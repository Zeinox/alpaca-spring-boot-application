package com.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class StockLookupService {
    @Value("${alpaca.secret}")
    String secretKey;

    @Value("${alpaca.key}")
    String key;

    @Value("${alpaca.url}")
    String url;

    public String lookupStock(String stockName)
    {
        RestTemplate restTemplate = new RestTemplate();
        String urlString = url + "/v1/assets/" + stockName;
        HttpHeaders headers = new HttpHeaders();

        headers.set("APCA-API-KEY-ID", key);
        headers.set("APCA-API-SECRET-KEY", secretKey);

        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        log.info("URL: " + urlString);
        String result = "";
        try{
             result = restTemplate.exchange( urlString , HttpMethod.GET, entity, String.class).getBody();
        }catch (HttpClientErrorException e)
        {
            log.error(e.toString());
            log.error(entity.toString());
        }


        return result;
    }

    public String lookupAccountInfo()
    {
        RestTemplate restTemplate = new RestTemplate();
        String urlString = url + "/v1/account";
        HttpHeaders headers = new HttpHeaders();

        headers.set("APCA-API-KEY-ID", key);
        headers.set("APCA-API-SECRET-KEY", secretKey);

        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        log.info("URL: " + urlString);
        String result = "Failed to connect...";
        try{
            result = restTemplate.exchange( urlString , HttpMethod.GET, entity, String.class).getBody();
        }catch (HttpClientErrorException e)
        {
            log.error(e.toString());
            log.error(entity.toString());
        }


        return result;
    }
}
