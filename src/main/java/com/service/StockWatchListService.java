package com.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.thymeleaf.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class StockWatchListService {
    @Value("${watch.list.path}")
    private String watchlistPath;

    @Autowired
    private StockLookupService stockLookupService;

    private List<String> watchlist = new ArrayList<>();

    @EventListener(ApplicationReadyEvent.class)
    private void loadWatchlist()
    {
        ObjectMapper mapper = new ObjectMapper();
        try {
            File file = ResourceUtils.getFile("classpath:" + watchlistPath);
            String content = new String(Files.readAllBytes(file.toPath()));
            if(!StringUtils.isEmpty(content)) watchlist = mapper.readValue(content, List.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addToWatchlist(String stockId)
    {
        stockId = stockId.toUpperCase();
        String lookupResults = stockLookupService.lookupStock(stockId);
        if(!watchlist.contains(stockId) && !StringUtils.isEmpty(lookupResults)) watchlist.add(stockId);
        writeWatchlistToFile(watchlist);
    }

    public void popWatchlist()
    {
        if(watchlist.size() > 0) watchlist.remove(watchlist.size()-1);
        writeWatchlistToFile(watchlist);
    }

    private void writeWatchlistToFile(List watchlist)
    {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String watchlistJson = mapper.writeValueAsString(watchlist);
            File file = ResourceUtils.getFile("classpath:" + watchlistPath);
            FileOutputStream outputStream = new FileOutputStream(file.getAbsoluteFile());
            byte[] strToBytes = watchlistJson.getBytes();
            outputStream.write(strToBytes);
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> getWatchlist()
    {
        return new ArrayList<>(watchlist);
    }
}
