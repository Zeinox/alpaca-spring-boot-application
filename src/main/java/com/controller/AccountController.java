package com.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.RssFeedView;
import com.service.StockWatchListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.servlet.View;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class AccountController {

    @Autowired
    private RssFeedView view;

    @Autowired
    private StockWatchListService stockWatchListService;

    @GetMapping("/rss")
    public View getFeed() {
        return view;
    }

    @GetMapping("/")
    public String redirectToAccount(){
        return "redirect:/account";
    }

    @GetMapping("/account")
    public String getAccountPage(Model model) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        model.addAttribute("watchlist", mapper.writerWithDefaultPrettyPrinter().writeValueAsString(stockWatchListService.getWatchlist()));
        return "main";
    }
}
