package com.controller;

import com.domain.Response;
import com.domain.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.StockLookupService;
import com.service.StockWatchListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.thymeleaf.util.ListUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
public class RSSController {

    @Autowired
    StockLookupService stockLookupService;

    @Autowired
    StockWatchListService stockWatchListService;

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @MessageMapping("/account")
    @SendTo("/topic/account")
    public Response pullAccountData(Message message) throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        String lookupReturn = stockLookupService.lookupAccountInfo();
        String stockLookupReturn = stockLookupService.lookupStock(message.getName());


        Map deserialize = mapper.readValue(lookupReturn, Map.class);
        Map stockDeserialize = mapper.readValue(stockLookupReturn, Map.class);

        String returnString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(deserialize) + "\n" +
                mapper.writerWithDefaultPrettyPrinter().writeValueAsString(stockDeserialize);

        return new Response(returnString);
    }

    @Scheduled(fixedDelay = 5000)
    private void postAccountData() throws Exception
    {
        ObjectMapper mapper = new ObjectMapper();

        String lookupReturn = stockLookupService.lookupAccountInfo();

        Map deserialize = mapper.readValue(lookupReturn, Map.class);

        String returnString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(deserialize);

        simpMessagingTemplate.convertAndSend("/topic/account", new Response(returnString));
    }

    @Scheduled(fixedDelay = 5000)
    private void postStockData() throws Exception
    {
        ObjectMapper mapper = new ObjectMapper();

        List<String> watchlist = stockWatchListService.getWatchlist();

        if(ListUtils.isEmpty(watchlist))
        {
            simpMessagingTemplate.convertAndSend("/topic/watchlistUpdates", new Response(""));
            return;
        }


        StringBuilder returnString = new StringBuilder();

        watchlist.stream().forEach(stockId ->
        {
            String lookupReturn = stockLookupService.lookupStock(stockId);

            try {
                Map deserialize = mapper.readValue(lookupReturn, Map.class);
                returnString.append(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(deserialize) + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        simpMessagingTemplate.convertAndSend("/topic/watchlistUpdates", new Response(returnString.toString()));
    }

    @MessageMapping("/addToWatchlist")
    @SendTo("/topic/watchlist")
    public Response addToWatchList(Message message){
        stockWatchListService.addToWatchlist(message.getName());
        return constructWatchlistReturn();
    }

    @MessageMapping("/popWatchlist")
    @SendTo("/topic/watchlist")
    public Response popWatchList(){
        stockWatchListService.popWatchlist();
        return constructWatchlistReturn();
    }

    private Response constructWatchlistReturn(){
        ObjectMapper mapper = new ObjectMapper();
        String returnString = null;
        try {
            returnString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(stockWatchListService.getWatchlist());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return new Response(returnString);
    }

}
